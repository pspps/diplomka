#include <stdlib.h>
#include <string.h>
#include "include/dh.h"
#include "ue_info.h"

struct ue_info_list {
	struct ue_info info;
	struct ue_info_list * next;
};

static struct ue_info_list * ue_list_top = NULL;



int remove_ue_internal(struct ue_info_list * info);

struct ue_info * add_new_ue()
{
	struct ue_info_list * new = malloc(sizeof(struct ue_info_list));
	memset(new, 0, sizeof(struct ue_info_list));

    if (ue_list_top)
	    ue_list_top->next = new;
    else
	    ue_list_top = new;

	return &new->info;
}

int remove_ue_by_cookie(uint8_t * i_cookie, uint8_t * r_cookie)
{
	struct ue_info_list * i = ue_list_top,
						* prev = NULL;
	while (i != NULL) {
		if (!memcmp(i_cookie, i->info.i_cookie, sizeof(i->info.i_cookie)) &&
			!memcmp(r_cookie, i->info.r_cookie, sizeof(i->info.r_cookie))) {
			if (prev) {
				prev->next = i->next;
			}
			else {
				ue_list_top = i->next;
			}
			return remove_ue_internal(i);
		}

		prev = i;
		i = i->next;
	}
	return -1;
}

struct ue_info * find_ue_by_cookie(uint8_t * i_cookie, uint8_t * r_cookie)
{
	struct ue_info_list * i = ue_list_top;
	while (i != NULL) {
		if (!memcmp(i_cookie, i->info.i_cookie, sizeof(i->info.i_cookie)) &&
			!memcmp(r_cookie, i->info.r_cookie, sizeof(i->info.r_cookie))) {
			return &i->info;
		}
		i = i->next;
	}
	return NULL;
}

struct ue_info * find_ue_by_icookie(uint8_t * i_cookie)
{
	struct ue_info_list * i = ue_list_top;
	while (i != NULL) {
		if (!memcmp(i_cookie, i->info.i_cookie, sizeof(i->info.i_cookie))) {
			return &i->info;
		}
		i = i->next;
	}
	return NULL;
}

int remove_ue_internal(struct ue_info_list * info)
{
	if (info->info.dh_mitmToEpdg)
		DH_free(info->info.dh_mitmToEpdg);
	if (info->info.dh_ueToMitm)
		DH_free(info->info.dh_ueToMitm);
	if (info->info.sharedSecret_ueToMitm)
		free(info->info.sharedSecret_ueToMitm);
	if (info->info.sharedSecret_mitmToEpdg)
		free(info->info.sharedSecret_mitmToEpdg);
	free(info);

	return 0;
}

int initInfo_byDh(struct ue_info * info)
{
	info->publicKey_mitm_ueToMitm_len = info->publicKey_peer_ueToMitm_len = info->sharedSecret_ueToMitm_len = get_DH_group_size(info->dh_ueToMitm);
	info->publicKey_mitm_mitmToEpdg_len = info->publicKey_peer_mitmToEpdg_len = info->sharedSecret_mitmToEpdg_len = get_DH_group_size(info->dh_mitmToEpdg);

	//TODO error handling
	info->publicKey_mitm_ueToMitm = malloc(info->publicKey_mitm_ueToMitm_len);
	memset(info->publicKey_mitm_ueToMitm, 0, info->publicKey_mitm_ueToMitm_len);
	info->publicKey_mitm_mitmToEpdg = malloc(info->publicKey_mitm_mitmToEpdg_len);
	memset(info->publicKey_mitm_mitmToEpdg, 0, info->publicKey_mitm_mitmToEpdg_len);
	info->publicKey_peer_ueToMitm = malloc(info->publicKey_peer_ueToMitm_len);
	memset(info->publicKey_peer_ueToMitm, 0, info->publicKey_peer_ueToMitm_len);
	info->publicKey_peer_mitmToEpdg = malloc(info->publicKey_peer_mitmToEpdg_len);
	memset(info->publicKey_peer_mitmToEpdg, 0, info->publicKey_peer_mitmToEpdg_len);
	info->sharedSecret_ueToMitm = malloc(info->sharedSecret_ueToMitm_len);
	memset(info->sharedSecret_ueToMitm, 0, info->sharedSecret_ueToMitm_len);
	info->sharedSecret_mitmToEpdg = malloc(info->sharedSecret_mitmToEpdg_len);
	memset(info->sharedSecret_mitmToEpdg, 0, info->sharedSecret_mitmToEpdg_len);

	get_DH_pubkey(info->dh_ueToMitm, info->publicKey_mitm_ueToMitm, &info->publicKey_mitm_ueToMitm_len);
	get_DH_pubkey(info->dh_mitmToEpdg, info->publicKey_mitm_mitmToEpdg, &info->publicKey_mitm_mitmToEpdg_len);

	return 0;
}
