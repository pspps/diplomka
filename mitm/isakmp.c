#include <arpa/inet.h>
#include <openssl/evp.h>
#include <stdlib.h>
#include "include/isakmp.h"

const id_name_map notification_map[] = { /* From RFC 2408 3.14.1 */
   {0, "UNSPECIFIED"},
   {1, "INVALID-PAYLOAD-TYPE"},
   {2, "DOI-NOT-SUPPORTED"},
   {3, "SITUATION-NOT-SUPPORTED"},
   {4, "INVALID-COOKIE"},
   {5, "INVALID-MAJOR-VERSION"},
   {6, "INVALID-MINOR-VERSION"},
   {7, "INVALID-EXCHANGE-TYPE"},
   {8, "INVALID-FLAGS"},
   {9, "INVALID-MESSAGE-ID"},
   {10, "INVALID-PROTOCOL-ID"},
   {11, "INVALID-SPI"},
   {12, "INVALID-TRANSFORM-ID"},
   {13, "ATTRIBUTES-NOT-SUPPORTED"},
   {14, "NO-PROPOSAL-CHOSEN"},
   {15, "BAD-PROPOSAL-SYNTAX"},
   {16, "PAYLOAD-MALFORMED"},
   {17, "INVALID-KEY-INFORMATION"},
   {18, "INVALID-ID-INFORMATION"},
   {19, "INVALID-CERT-ENCODING"},
   {20, "INVALID-CERTIFICATE"},
   {21, "CERT-TYPE-UNSUPPORTED"},
   {22, "INVALID-CERT-AUTHORITY"},
   {23, "INVALID-HASH-INFORMATION"},
   {24, "AUTHENTICATION-FAILED"},
   {25, "INVALID-SIGNATURE"},
   {26, "ADDRESS-NOTIFICATION"},
   {27, "NOTIFY-SA-LIFETIME"},
   {28, "CERTIFICATE-UNAVAILABLE"},
   {29, "UNSUPPORTED-EXCHANGE-TYPE"},
   {30, "UNEQUAL-PAYLOAD-LENGTHS"},
   {9101, "Checkpoint-Firewall-1"},
   {9110, "Checkpoint-Firewall-1"},
   {24576, "RESPONDER-LIFETIME"},	/* Next 3 are from RFC 2407 4.6.3 */
   {24577, "REPLAY-STATUS"},
   {24578, "INITIAL-CONTACT"},
   {-1, NULL}
};
const id_name_map notification_map2[] = { /* From RFC 5996 3.10.1 */
   {0, "RESERVED"},
   {1, "UNSUPPORTED_CRITICAL_PAYLOAD"},
   {4, "INVALID_IKE_SPI"},
   {5, "INVALID_MAJOR_VERSION"},
   {7, "INVALID_SYNTAX"},
   {9, "INVALID_MESSAGE_ID"},
   {11, "INVALID_SPI"},
   {14, "NO_PROPOSAL_CHOSEN"},
   {17, "INVALID_KE_PAYLOAD"},
   {24, "AUTHENTICATION_FAILED"},
   {34, "SINGLE_PAIR_REQUIRED"},
   {35, "NO_ADDITIONAL_SAS"},
   {36, "INTERNAL_ADDRESS_FAILURE"},
   {37, "FAILED_CP_REQUIRED"},
   {38, "TS_UNACCEPTABLE"},
   {39, "INVALID_SELECTORS"},
   {43, "TEMPORARY_FAILURE"},
   {44, "CHILD_SA_NOT_FOUND"},
   {9101, "Checkpoint-Firewall-1"},
   {9110, "Checkpoint-Firewall-1"},
   {16384, "INITIAL_CONTACT"},
   {16385, "SET_WINDOW_SIZE"},
   {16386, "ADDITIONAL_TS_POSSIBLE"},
   {16387, "IPCOMP_SUPPORTED"},
   {16388, "NAT_DETECTION_SOURCE_IP"},
   {16389, "NAT_DETECTION_DESTINATION_IP"},
   {16390, "COOKIE"},
   {16391, "USE_TRANSPORT_MODE"},
   {16392, "HTTP_CERT_LOOKUP_SUPPORTED"},
   {16393, "REKEY_SA"},
   {16394, "ESP_TFC_PADDING_NOT_SUPPORTED"},
   {16395, "NON_FIRST_FRAGMENTS_ALSO"},
   {-1, NULL}
};
const id_name_map attr_map[] = {	/* From RFC 2409 App. A and */
   {1, "Enc"},			/* draft-ietf-ipsec-isakmp-gss-auth */
   {2, "Hash"},
   {3, "Auth"},
   {4, "Group"},
   {5, "GroupType"},
   {6, "GroupPrime/IrreduciblePolynomial"},
   {7, "GroupGeneratorOne"},
   {8, "GroupGeneratorTwo"},
   {9, "GroupCurve A"},
   {10, "GroupCurve B"},
   {11, "LifeType"},
   {12, "LifeDuration"},
   {13, "PRF"},
   {14, "KeyLength"},
   {15, "FieldSize"},
   {16, "GroupOrder"},
   {16384, "GSSIdentityName"},
   {-1, NULL}
};
const id_name_map trans_type_map[] = {	/* From RFC 5996 3.3.2 */
   {1, "Encr"},
   {2, "Prf"},
   {3, "Integ"},
   {4, "DH_Group"},
   {5, "ESN"},
   {-1, NULL}
};
const id_name_map enc_map[] = {	/* From RFC 2409 App. A */
   {1, "DES"},
   {2, "IDEA"},
   {3, "Blowfish"},
   {4, "RC5"},
   {5, "3DES"},
   {6, "CAST"},
   {7, "AES"},		/* RFC 3602 */
   {8, "Camellia"},	/* RFC 4312 */
   {65001, "Mars"},	/* Defined in strongSwan constants.h */
   {65002, "RC6"},	/* Defined in strongSwan constants.h */
   {65003, "ID_65003"},	/* Defined in strongSwan constants.h */
   {65004, "Serpent"},	/* Defined in strongSwan constants.h */
   {65005, "Twofish"},	/* Defined in strongSwan constants.h */
   {-1, NULL}
};
const id_name_map encr_map[] = {	/* From RFC 5996 (IKEv2) 3.3.2 */
   {1, "DES_IV64"},
   {2, "DES"},
   {3, "3DES"},
   {4, "RC5"},
   {5, "IDEA"},
   {6, "CAST"},
   {7, "Blowfish"},
   {8, "3IDEA"},
   {9, "DES_IV32"},
   {11, "NULL"},
   {12, "AES_CBC"},
   {13, "AES_CTR"},
   {14, "AES_CCM_ICV8"},		/* RFC 5282 */
   {15, "AES_CCM_ICV12"},		/* RFC 5282 */
   {16, "AES_CCM_ICV16"},		/* RFC 5282 */
   {18, "AES_GCM_ICV8"},		/* RFC 5282 */
   {19, "AES_GCM_ICV12"},		/* RFC 5282 */
   {20, "AES_GCM_ICV16"},		/* RFC 5282 */
   {23, "CAMELLIA_CBC"},		/* RFC 5996 */
   {-1, NULL}
};
const id_name_map hash_map[] = {	/* From RFC 2409 App. A */
   {1, "MD5"},
   {2, "SHA1"},
   {3, "Tiger"},
   {4, "SHA2-256"},
   {5, "SHA2-384"},
   {6, "SHA2-512"},
   {-1, NULL}
};
const id_name_map prf_map[] = {		/* From RFC 5996 3.3.2 */
   {1, "HMAC_MD5"},
   {2, "HMAC_SHA1"},
   {3, "HMAC_TIGER"},
   {4, "AES128_XCBC"},		/* RFC 4434 */
   {5, "HMAC_SHA2_256"},	/* RFC 4868 */
   {6, "HMAC_SHA2_384"},	/* RFC 4868 */
   {7, "HMAC_SHA2_512"},	/* RFC 4868 */
   {8, "HMAC_AES128_CMAC"},	/* RFC 4615 */
   {-1, NULL}
};
const id_name_map auth_map[] = {	/* From RFC 2409 App. A */
   {1, "PSK"},
   {2, "DSS"},
   {3, "RSA_Sig"},
   {4, "RSA_Enc"},
   {5, "RSA_RevEnc"},
   {6, "ElGamel_Enc"},
   {7, "ElGamel_RevEnc"},
   {8, "ECDSA_Sig"},
   {9, "ECDSA_SHA256"},		/* RFC 4754 */
   {10, "ECDSA_SHA384"},	/* RFC 4754 */
   {11, "ECDSA_SHA512"},	/* RFC 4754 */
   {128, "CRACK"},		/* draft-harkins-ipsra-crack-00 */
   {64221, "Hybrid_RSA"},	/* draft-ietf-ipsec-isakmp-hybrid-auth-05 */
   {64223, "Hybrid_DSS"},	/* draft-ietf-ipsec-isakmp-hybrid-auth-05 */
   {65001, "XAUTH_PSK"},	/* draft-ietf-ipsec-isakmp-xauth-06 */
   {65003, "XAUTH_DSS"},	/* draft-ietf-ipsec-isakmp-xauth-06 */
   {65005, "XAUTH_RSA"},	/* draft-ietf-ipsec-isakmp-xauth-06 */
   {65007, "XAUTH_RSA_Enc"},	/* draft-ietf-ipsec-isakmp-xauth-06 */
   {65009, "XAUTH_RSA_RevEnc"},	/* draft-ietf-ipsec-isakmp-xauth-06 */
   {-1, NULL}
};
const id_name_map integ_map[] = {	/* From RFC 5996 3.3.2 */
   {1, "HMAC_MD5_96"},
   {2, "HMAC_SHA1_96"},
   {3, "DES_MAC"},
   {4, "KPDK_MD5"},
   {5, "AES_XCBC_96"},
   {6, "HMAC_MD5_128"},		/* RFC 4595 */
   {7, "HMAC_SHA1_160"},	/* RFC 4595 */
   {8, "AES_CMAC_96"},		/* RFC 4494 */
   {9, "AES_128_GMAC"},		/* RFC 4543 */
   {10, "AES_192_GMAC"},	/* RFC 4543 */
   {11, "AES_256_GMAC"},	/* RFC 4543 */
   {12, "HMAC_SHA2_256_128"},	/* RFC 4868 */
   {13, "HMAC_SHA2_384_192"},	/* RFC 4868 */
   {14, "HMAC_SHA2_512_256"},	/* RFC 4868 */
   {-1, NULL}
};
const id_name_map dh_map[] = {	/* From RFC 2409 App. A */
   {1, "1:modp768"},
   {2, "2:modp1024"},
   {3, "3:ec2n155"},
   {4, "4:ec2n185"},
   {5, "5:modp1536"},	/* RFC 3526 */
   {6, "6:ec2n163"},
   {7, "7:ec2n163"},
   {8, "8:ec2n283"},
   {9, "9:ec2n283"},
   {10, "10:ec2n409"},
   {11, "11:ec2n409"},
   {12, "12:ec2n571"},
   {13, "13:ec2n571"},
   {14, "14:modp2048"},	/* RFC 3526 */
   {15, "15:modp3072"},	/* RFC 3526 */
   {16, "16:modp4096"},	/* RFC 3526 */
   {17, "17:modp6144"},	/* RFC 3526 */
   {18, "18:modp8192"},	/* RFC 3526 */
   {19, "19:ecp256"},	/* RFC 5903 */
   {20, "20:ecp384"},	/* RFC 5903 */
   {21, "21:ecp521"},	/* RFC 5903 */
   {22, "22:modp1024s160"},	/* RFC 5114 */
   {23, "23:modp2048s224"},	/* RFC 5114 */
   {24, "24:modp2048s256"},	/* RFC 5114 */
   {25, "25:ecp192"},	/* RFC 5114 */
   {26, "26:ecp224"},	/* RFC 5114 */
   {27, "27:brainpoolP224r1"},	/* RFC 6954 */
   {28, "28:brainpoolP256r1"},	/* RFC 6954 */
   {29, "29:brainpoolP384r1"},	/* RFC 6954 */
   {30, "30:brainpoolP512r1"},	/* RFC 6954 */
   {-1, NULL}
};
const id_name_map life_map[] = {	/* From RFC 2409 App. A */
   {1, "Seconds"},
   {2, "Kilobytes"},
   {-1, NULL}
};
const id_name_map payload_map[] = {	/* Payload types from RFC 2408 3.1 */
   {1, "SecurityAssociation"},		/* and RFC 4306 3.2 */
   {2, "Proposal"},
   {3, "Transform"},
   {4, "KeyExchange"},
   {5, "Identification"},
   {6, "Certificate"},
   {7, "CertificateRequest"},
   {8, "Hash"},
   {9, "Signature"},
   {10, "Nonce"},
   {11, "Notification"},
   {12, "Delete"},
   {13, "VendorID"},
   {20, "NAT-D"},		/* RFC 3947 NAT Discovery */
   {33, "SecurityAssociation"},	/* Values 33-48 are from RFC 5996 IKEv2 */
   {34, "KeyExchange"},
   {35, "IDI"},
   {36, "IDR"},
   {37, "Certificate"},
   {38, "CertificateRequest"},
   {39, "AUTH"},
   {40, "Nonce"},
   {41, "Notification"},
   {42, "Delete"},
   {43, "VendorID"},
   {44, "TSI"},
   {45, "TSR"},
   {46, "Encrypted"},
   {47, "Configuration"},
   {48, "EAP"},
   {49, "GSPM"},		/* RFC 6467 */
   {-1, NULL}
};
const id_name_map doi_map[] = {
   {0, "ISAKMP"},
   {1, "IPsec"},
   {2, "GDOI"},		/* RFC 6407 */
   {-1, NULL}
};
const id_name_map protocol_map[] = {
   {1, "PROTO_ISAKMP"},
   {2, "PROTO_IPSEC_AH"},
   {3, "PROTO_IPSEC_ESP"},
   {4, "PROTO_IPSEC_COMP"},
   {-1, NULL}
};
const id_name_map id_map[] = {	/* From RFC 2407 4.6.2.1 */
   {1, "ID_IPV4_ADDR"},
   {2, "ID_FQDN"},
   {3, "ID_USER_FQDN"},
   {4, "ID_IPV4_ADDR_SUBNET"},
   {5, "ID_IPV6_ADDR"},
   {6, "ID_IPV6_ADDR_SUBNET"},
   {7, "ID_IPV4_ADDR_RANGE"},
   {8, "ID_IPV6_ADDR_RANGE"},
   {9, "ID_DER_ASN1_DN"},
   {10, "ID_DER_ASN1_GN"},
   {11, "ID_KEY_ID"},
};
const id_name_map cert_map[] = {	/* From RFC 2408 Sec. 3.9 */
   {1, "PKCS #7 wrapped X.509 certificate"},
   {2, "PGP Certificate"},
   {3, "DNS Signed Key"},
   {4, "X.509 Certificate - Signature"},
   {5, "X.509 Certificate - Key Exchange"},
   {6, "Kerberos Tokens"},
   {7, "Certificate Revocation List (CRL)"},
   {8, "Authority Revocation List (ARL)"},
   {9, "SPKI Certificate"},
   {10, "X.509 Certificate - Attribute"},
   {-1, NULL}
};
const id_name_map exchange_type[] =	/* From RVFC 7296 Sec. 3.1 */
{
   {34, "IKE_SA_INIT"},
   {35, "IKE_AUTH"},
   {36, "CREATE_CHILD_SA"},
   {37, "INFORMATIONAL"},
};

const char * find_entry(int id, const id_name_map * list, int nlist_size)
{
	int i;
	for (i = 0; i < nlist_size; ++i) {
		if (list[i].id == id)
			return list[i].name;
	}
	return NULL;
}

const char * exchange_type_to_string(int id)
{
	const char * ret = find_entry(id, exchange_type, NELEMS(exchange_type));
	if (ret == NULL)
		return "Unknown exchange type";
	return ret;
}

const char * payload_type_to_string(int id)
{
	const char * ret = find_entry(id, payload_map, NELEMS(payload_map));
	if (ret == NULL)
		return "Unknown exchange type";
	return ret;
}

EVP_MD * encTransform_to_evpMd(struct isakmp_transform2 * transform)
{
	switch (htons(transform->isat2_transid)) {
/*
1 	ENCR_DES_IV64 	UNSPECIFIED 	-
2 	ENCR_DES 	[RFC2405] 	[RFC7296]
3 	ENCR_3DES 	[RFC2451] 	[RFC7296]
4 	ENCR_RC5 	[RFC2451] 	[RFC7296]
5 	ENCR_IDEA 	[RFC2451] 	[RFC7296]
6 	ENCR_CAST 	[RFC2451] 	[RFC7296]
7 	ENCR_BLOWFISH 	[RFC2451] 	[RFC7296]
8 	ENCR_3IDEA 	UNSPECIFIED 	[RFC7296]
9 	ENCR_DES_IV32 	UNSPECIFIED 	-
10 	Reserved 	[RFC7296] 	-
11 	ENCR_NULL 	[RFC2410] 	Not allowed
12 	ENCR_AES_CBC 	[RFC3602] 	[RFC7296]
13 	ENCR_AES_CTR 	[RFC3686] 	[RFC5930]
14 	ENCR_AES_CCM_8 	[RFC4309] 	[RFC5282]
15 	ENCR_AES_CCM_12 	[RFC4309] 	[RFC5282]
16 	ENCR_AES_CCM_16 	[RFC4309] 	[RFC5282]
17 	Unassigned 		
18 	ENCR_AES_GCM_8 	[RFC4106] [RFC8247] 	[RFC5282] [RFC8247]
19 	ENCR_AES_GCM_12 	[RFC4106] [RFC8247] 	[RFC5282] [RFC8247]
20 	ENCR_AES_GCM_16 	[RFC4106] [RFC8247] 	[RFC5282] [RFC8247]
21 	ENCR_NULL_AUTH_AES_GMAC 	[RFC4543] 	Not allowed
22 	Reserved for IEEE P1619 XTS-AES 	[Matt_Ball] 	-
23 	ENCR_CAMELLIA_CBC 	[RFC5529] 	[RFC7296]
24 	ENCR_CAMELLIA_CTR 	[RFC5529] 	-
25 	ENCR_CAMELLIA_CCM_8 	[RFC5529] [RFC8247] 	-
26 	ENCR_CAMELLIA_CCM_12 	[RFC5529] [RFC8247] 	-
27 	ENCR_CAMELLIA_CCM_16 	[RFC5529] [RFC8247] 	-
28 	ENCR_CHACHA20_POLY1305 	[RFC7634] 	[RFC7634] */
		case 12:
			if ()
		default:
			printf("ERROR: unsupported encryption transform '%d'\n", transform->isat2_transid);
			return NULL;

	}
}
