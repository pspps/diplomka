#ifndef DH_H
#define DH_H
#include <openssl/dh.h>
#include <stdint.h>


DH * get_DH_by_num(int num);
void free_dh(DH * dh);

int get_DH_pubkey(DH * dh, uint8_t * peer_pub, int * peer_pub_len);
int generate_DH_shared_secret(DH * dh, uint8_t * peer_pub, int peer_pub_len, uint8_t * shared_secret, int * shared_secret_len);

int get_DH_group_size(DH * dh);


#endif
