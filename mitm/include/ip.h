#ifndef IP_H
#define IP_H

#include <stdint.h>

struct iphdr
  {
#ifdef WORDS_BIGENDIAN
    unsigned int version:4;
    unsigned int ihl:4;
#else
    unsigned int ihl:4;
    unsigned int version:4;
#endif
    uint8_t tos;
    uint16_t tot_len;
    uint16_t id;
    uint16_t frag_off;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t check;
    uint32_t saddr;
    uint32_t daddr;
    /*The options start here. */
  };

struct udphdr {
  uint16_t	source;
  uint16_t	dest;
  uint16_t	len;
  uint16_t	check;
};

#endif
