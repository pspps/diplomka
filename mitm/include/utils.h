#ifndef UTILS_H
#define UTILS_H
#include <stdint.h>

int print_hex(const uint8_t * buf, int buf_len);

#endif
