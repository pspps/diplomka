#include <stdint.h>
#include <stdio.h>
#include "include/utils.h"

int print_hex(const uint8_t * buf, int buf_len)
{
	const uint8_t * ptr = buf;
	while (ptr < buf + buf_len) {
		printf("%hhx", *ptr);
		ptr++;
	}
	return 0;
}
