#ifndef UE_INFO_H
#define UE_INFO_H
#include <openssl/dh.h>
#include <openssl/evp.h>
#include <stdint.h>
#include "include/isakmp.h"

struct ue_info {
	uint8_t i_cookie[COOKIE_SIZE_BYTES];
	uint8_t r_cookie[COOKIE_SIZE_BYTES];
	DH * dh_ueToMitm;
	DH * dh_mitmToEpdg;
	uint8_t * publicKey_mitm_ueToMitm;
	uint8_t * publicKey_mitm_mitmToEpdg;
	uint8_t * publicKey_peer_ueToMitm;
	uint8_t * publicKey_peer_mitmToEpdg;
	uint8_t * sharedSecret_ueToMitm;
	uint8_t * sharedSecret_mitmToEpdg;
	int publicKey_mitm_ueToMitm_len;
	int publicKey_mitm_mitmToEpdg_len;
	int publicKey_peer_ueToMitm_len;
	int publicKey_peer_mitmToEpdg_len;
	int sharedSecret_ueToMitm_len;
	int sharedSecret_mitmToEpdg_len;

	EVP_CIPHER * cipher;
	EVP_MD * auth_hmac_hash;
	EVP_MD * prng_hmac_hash;
};

struct ue_info * add_new_ue();
int remove_ue_by_cookie(uint8_t * i_cookie, uint8_t * r_cookie);
struct ue_info * find_ue_by_cookie(uint8_t * i_cookie, uint8_t * r_cookie);
struct ue_info * find_ue_by_icookie(uint8_t * i_cookie);
int initInfo_byDh(struct ue_info * info);

#endif
