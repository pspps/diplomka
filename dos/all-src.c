#include <arpa/inet.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <net/if.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "isakmp.h"

#define DEFAULT_NONCE_LEN 20	/* Default Nonce length in bytes */
#define MAXUDP 65507			/* Max UDP data size = 64k - 20 - 8 */
#define DEFAULT_TIMEOUT 5000	/* Default per-host timeout in ms */

#define PROTO_ISAKMP             1
#define PROTO_IPSEC_AH           2
#define PROTO_IPSEC_ESP          3
#define PROTO_IPCOMP             4

#define UNSUPPORTED_CRITICAL_PAYLOAD              1
#define INVALID_IKE_SPI                           4
#define INVALID_MAJOR_VERSION                     5
#define INVALID_SYNTAX                            7
#define INVALID_MESSAGE_ID                        9
#define INVALID_SPI                              11
#define NO_PROPOSAL_CHOSEN                       14
#define INVALID_KE_PAYLOAD                       17
#define AUTHENTICATION_FAILED                    24
#define SINGLE_PAIR_REQUIRED                     34
#define NO_ADDITIONAL_SAS                        35
#define INTERNAL_ADDRESS_FAILURE                 36
#define FAILED_CP_REQUIRED                       37
#define TS_UNACCEPTABLE                          38
#define INVALID_SELECTORS                        39
#define TEMPORARY_FAILURE                        43
#define CHILD_SA_NOT_FOUND                       44


int mbz_value=0;			/* Value for MBZ fields */

typedef struct {	/* IKE Packet Parameters */
   unsigned char *icookie_data;	/* Initiator cookie */
   size_t icookie_data_len;
   unsigned char *rcookie_data;	/* Responder cookie */
   size_t rcookie_data_len;
   unsigned exchange_type;
   unsigned hdr_next_payload;	/* Next payload in ISAKMP header */
   int header_version;	/* ISAKMP Header Version */
   unsigned protocol;	/* Proposal protocol */
   unsigned spi_size;	/* Proposal SPI Size */
   int hdr_flags;	/* ISAKMP Header flags */
   unsigned hdr_msgid;	/* ISAKMP Header message id */
   char *header_length;

   int sa_presence;
   unsigned dhgroup;

   int ke_presence;

   int nonce_presence;
   size_t nonce_data_len;

   unsigned char *cr_data;
   size_t cr_data_len;
   int vendor_id_flag;

   int invalid_ke_presence;
   int invalid_ke_dh;
} ike_packet_params;

ike_packet_params sa_params = {
    .dhgroup           = 2,
    .vendor_id_flag    = 0,
    .exchange_type     = ISAKMP_XCHG_IKE_SA_INIT,
    .nonce_presence    = 1,
    .nonce_data_len    = DEFAULT_NONCE_LEN,
    .ke_presence       = 1,
    .header_length     = NULL,
    .cr_data           = NULL,
    .cr_data_len       = 0,
    .header_version    = 2<<4,
    .protocol          = PROTO_ISAKMP,
    .spi_size          = 0,
    .hdr_flags         = 0x08,
    .hdr_msgid         = 0,
    .hdr_next_payload  = 0,
    .icookie_data      = NULL,
    .icookie_data_len  = 0,
    .rcookie_data      = NULL,
    .rcookie_data_len  = 0,
	.sa_presence       = 1,
};

ike_packet_params invalid_ke_params = {
    .dhgroup             = 2,
    .exchange_type       = ISAKMP_XCHG_IKE_SA_INIT,
    .header_length       = NULL,
    .cr_data             = NULL,
    .cr_data_len         = 0,
    .header_version      = 2<<4,
    .protocol            = PROTO_ISAKMP,
    .spi_size            = 0,
    .hdr_flags           = 0x08,
    .hdr_msgid           = 0,
    .hdr_next_payload    = 0,
    .icookie_data        = NULL,
    .icookie_data_len    = 0,
    .rcookie_data        = NULL,
    .rcookie_data_len    = 0,
	.invalid_ke_presence = 1,
	.invalid_ke_dh       = 2,
};

struct udphdr {
  uint16_t	source;
  uint16_t	dest;
  uint16_t	len;
  uint16_t	check;
};

/* rsh addition */
struct pseudo_hdr {  /* For computing UDP checksum */
   uint32_t	src_addr;
   uint32_t	dst_addr;
   uint8_t	mbz;
   uint8_t	proto;
   uint16_t	length;
};

struct iphdr
  {
#ifdef WORDS_BIGENDIAN
    unsigned int version:4;
    unsigned int ihl:4;
#else
    unsigned int ihl:4;
    unsigned int version:4;
#endif
    uint8_t tos;
    uint16_t tot_len;
    uint16_t id;
    uint16_t frag_off;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t check;
    uint32_t saddr;
    uint32_t daddr;
    /*The options start here. */
  };

void
err_sys(const char *fmt,...) {
   va_list ap;

   va_start(ap, fmt);
   vfprintf(stderr, fmt, ap);
   fprintf(stderr, ": %s\n", strerror(errno));
   va_end(ap);
   exit(EXIT_FAILURE);
}

void
err_msg(const char *fmt,...) {
   va_list ap;

   va_start(ap, fmt);
   vfprintf(stderr, fmt, ap);
   fprintf(stderr, "\n");
   va_end(ap);
}

void
warn_msg(const char *fmt,...) {
   va_list ap;

   va_start(ap, fmt);
   vfprintf(stderr, fmt, ap);
   fprintf(stderr, ": %s\n", strerror(errno));
   va_end(ap);
}

void *Malloc(size_t size) {
   void *result;

   result = malloc(size);

   if (result == NULL)
      err_sys("malloc");

   return result;
}

void *Realloc(void *ptr, size_t size) {
   void *result;

   result=realloc(ptr, size);

   if (result == NULL)
      err_sys("realloc");

   return result;
}

unsigned long int Strtoul(const char *nptr, int base) {
   char *endptr;
   unsigned long int result;

   result=strtoul(nptr, &endptr, base);
   if (endptr == nptr)	/* No digits converted */
      err_msg("ERROR: \"%s\" is not a valid numeric value", nptr);
   if (*endptr != '\0' && !isspace((unsigned char)*endptr))
      err_msg("ERROR: \"%s\" is not a valid numeric value", nptr);

   return result;
}

size_t
strlcpy(char *dst, const char *src, size_t siz)
{
        char *d = dst;
        const char *s = src;
        size_t n = siz;

        /* Copy as many bytes as will fit */
        if (n != 0) {
                while (--n != 0) {
                        if ((*d++ = *s++) == '\0')
                                break;
                }
        }

        /* Not enough room in dst, add NUL and traverse rest of src */
        if (n == 0) {
                if (siz != 0)
                        *d = '\0';                /* NUL-terminate dst */
                while (*s++)
                        ;
        }

        return(s - src - 1);        /* count does not include NUL */
}

char *
vmake_message(const char *fmt, va_list ap) {
   int n;
   /* Guess we need no more than 100 bytes. */
   size_t size = 100;
   char *p;
   p = Malloc (size);
   while (1) {
      /* Try to print in the allocated space. */
      n = vsnprintf (p, size, fmt, ap);
      /* If that worked, return the string. */
      if (n > -1 && n < (int) size)
         return p;
      /* Else try again with more space. */
      if (n > -1)    /* glibc 2.1 */
         size = n+1; /* precisely what is needed */
      else           /* glibc 2.0 */
         size *= 2;  /* twice the old size */
      p = Realloc (p, size);
   }
}

char *
make_message(const char *fmt, ...) {
   char * ret;
   va_list ap;
   va_start(ap, fmt);
   ret = vmake_message(fmt, ap);
   va_end(ap);
   return ret;
}

char *
append_message(char *old, const char *fmt, ...) {
   char *ret, *new;
   va_list ap;
   va_start(ap, fmt);
   new = vmake_message(fmt, ap);
   va_end(ap);
   ret = make_message("%s%s", old, new);
   free(old);
   free(new);
   return ret;
}

char *
hexstring(const unsigned char *data, size_t size) {
   char *result;
   char *r;
   const unsigned char *cp;
   unsigned i;
/*
 *	If the input data is NULL, return an empty string.
 */
   if (data == NULL) {
      result = Malloc(1);
      result[0] = '\0';
      return result;
   }
/*
 *	Create and return hex string.
 */
   result = Malloc(2*size + 1);
   cp = data;
   r = result;
   for (i=0; i<size; i++) {
      snprintf(r, 3, "%.2x", *cp++);
      r += 2;
   }
   *r = '\0';

   return result;
}

uint16_t
in_cksum(uint16_t *ptr, size_t nbytes) {

   register uint32_t sum;
   uint16_t oddbyte;
   register uint16_t answer;

/*
 * Our algorithm is simple, using a 32-bit accumulator (sum),
 * we add sequential 16-bit words to it, and at the end, fold back
 * all the carry bits from the top 16 bits into the lower 16 bits.
 */

   sum = 0;
   while (nbytes > 1)  {
      sum += *ptr++;
      nbytes -= 2;
   }

/* mop up an odd byte, if necessary */
   if (nbytes == 1) {
      oddbyte = 0;            /* make sure top half is zero */
      *((u_char *) &oddbyte) = *(u_char *)ptr;   /* one byte only */
      sum += oddbyte;
   }

/*
 * Add back carry outs from top 16 bits to low 16 bits.
 */

   sum  = (sum >> 16) + (sum & 0xffff);    /* add high-16 to low-16 */
   sum += (sum >> 16);                     /* add carry */
   answer = ~sum;          /* ones-complement, then truncate to 16 bits */
   return(answer);
}

unsigned char*
make_cr(size_t *length, unsigned next, unsigned char *cr_data,
        size_t cr_data_len) {
   unsigned char *payload;
   struct isakmp_generic* hdr;

   payload = Malloc(sizeof(struct isakmp_generic)+cr_data_len);
   hdr = (struct isakmp_generic*) payload;
   memset(hdr, mbz_value, sizeof(struct isakmp_generic));

   hdr->isag_np = next;		/* Next payload type */
   hdr->isag_length = htons(sizeof(struct isakmp_generic)+cr_data_len);

   memcpy(payload+sizeof(struct isakmp_generic), cr_data, cr_data_len);
   *length = sizeof(struct isakmp_generic) + cr_data_len;

   return payload;
}

unsigned char*
make_vid(size_t *length, unsigned next, unsigned char *vid_data,
         size_t vid_data_len) {
   unsigned char *payload;
   struct isakmp_vid* hdr;

   payload = Malloc(sizeof(struct isakmp_vid)+vid_data_len);
   hdr = (struct isakmp_vid*) payload;	/* Overlay vid struct on payload */
   memset(hdr, mbz_value, sizeof(struct isakmp_vid));

   hdr->isavid_np = next;		/* Next payload type */
   hdr->isavid_length = htons(sizeof(struct isakmp_vid)+vid_data_len);

   memcpy(payload+sizeof(struct isakmp_vid), vid_data, vid_data_len);
   *length = sizeof(struct isakmp_vid) + vid_data_len;

   return payload;
}

unsigned char*
add_vid(int finished, size_t *length, unsigned char *vid_data,
        size_t vid_data_len, int ike_version, unsigned next) {
   static int first_vid = 1;
   static unsigned char *vid_start=NULL;	/* Start of set of VIDs */
   static size_t cur_offset;			/* Start of current VID */
   static size_t end_offset;			/* End of VIDs */
   unsigned char *vid;				/* VID payload */
   size_t len;					/* VID length */
/*
 * Construct a VID if we are not finalising.
 */
   if (!finished) {
      vid = make_vid(&len, ISAKMP_NEXT_V2_VID, vid_data, vid_data_len);
      if (first_vid) {
         cur_offset = 0;
         end_offset = len;
         vid_start = Malloc(end_offset);
         memcpy(vid_start, vid, len);
         first_vid = 0;
      } else {
         cur_offset = end_offset;
         end_offset += len;
         vid_start = Realloc(vid_start, end_offset);
         memcpy(vid_start+cur_offset, vid, len);
      }
      return NULL;
   } else {
      struct isakmp_vid* hdr =
         (struct isakmp_vid*) (vid_start+cur_offset);   /* Overlay */

      hdr->isavid_np = next;
      *length = end_offset;
      return vid_start;
   }
}

unsigned char*
make_notify_error(size_t *length, unsigned next, int type, uint8_t *Ipayload, size_t Ipayload_len) {
   unsigned char *payload;
   struct isakmp_notification2* hdr;
   unsigned char *cp;

   if (!Ipayload)
	   Ipayload_len = 0;

   payload = Malloc(sizeof(struct isakmp_notification2)+Ipayload_len);
   hdr = (struct isakmp_notification2*) payload;  /* Overlay nonce struct on payload */
   memset(hdr, mbz_value, sizeof(struct isakmp_notification2));

   hdr->isan2_np = next;		/* Next payload type */
   hdr->isan2_length = htons(sizeof(struct isakmp_notification2)+Ipayload_len);

   cp = payload+sizeof(struct isakmp_notification2);

   memcpy(cp, Ipayload, Ipayload_len);

   *length = sizeof(struct isakmp_notification2)+Ipayload_len;
   return payload;
}

unsigned char*
make_notify_invalid_ke(size_t *length, unsigned next, int dh) {
	uint8_t payload[] = {0, 2};
	return make_notify_error(length, next, INVALID_KE_PAYLOAD, payload, sizeof(payload));
}

uint8_t
random_byte(void) {
	//TODO
	return random()%256;
}

unsigned char*
make_nonce(size_t *length, unsigned next, size_t nonce_len) {
   unsigned char *payload;
   struct isakmp_nonce* hdr;
   unsigned char *cp;
   unsigned i;

   payload = Malloc(sizeof(struct isakmp_nonce)+nonce_len);
   hdr = (struct isakmp_nonce*) payload;  /* Overlay nonce struct on payload */
   memset(hdr, mbz_value, sizeof(struct isakmp_nonce));

   hdr->isanonce_np = next;		/* Next payload type */
   hdr->isanonce_length = htons(sizeof(struct isakmp_nonce)+nonce_len);

   cp = payload+sizeof(struct isakmp_vid);
   for (i=0; i<nonce_len; i++)
      *(cp++) = (unsigned char) random_byte();

   *length = sizeof(struct isakmp_nonce)+nonce_len;
   return payload;
}

unsigned char*
make_ke2(size_t *length, unsigned next, unsigned dh_group, size_t kx_data_len) {
   unsigned char *payload;
   struct isakmp_kx2* hdr;
   unsigned char *kx_data;
   unsigned i;

   if (kx_data_len % 4)
      err_msg("Key exchange data length %d is not a multiple of 4",
              kx_data_len);

   payload = Malloc(sizeof(struct isakmp_kx2)+kx_data_len);
   hdr = (struct isakmp_kx2*) payload;	/* Overlay kx struct on payload */
   memset(hdr, mbz_value, sizeof(struct isakmp_kx2));

   kx_data = payload + sizeof(struct isakmp_kx2);
   for (i=0; i<kx_data_len; i++)
      *(kx_data++) = (unsigned char) random_byte();

   hdr->isakx2_np = next;		/* Next payload type */
   hdr->isakx2_length = htons(sizeof(struct isakmp_kx2)+kx_data_len);
   hdr->isakx2_dhgroup = htons(dh_group);

   *length = sizeof(struct isakmp_kx2) + kx_data_len;

   return payload;
}

unsigned char *
make_attr(size_t *outlen, int type, unsigned class, size_t length,
          unsigned b_value, void *v_value) {
   struct isakmp_attribute *hdr;
   unsigned char *cp;
   size_t total_len;

   total_len = sizeof(struct isakmp_attribute);
   if (type == 'V')
      total_len += length;

   cp = Malloc(total_len);
   hdr = (struct isakmp_attribute *) cp;
   memset(hdr, mbz_value, sizeof(struct isakmp_attribute));

   if (type == 'B') {	/* Basic Attribute */
      hdr->isaat_af_type = htons(class | 0x8000);
      hdr->isaat_lv = htons(b_value);
   } else {		/* Variable Attribute */
      hdr->isaat_af_type = htons(class);
      hdr->isaat_lv = htons(length);
      memcpy(cp+sizeof(struct isakmp_attribute), v_value, length);
   }

   *outlen = total_len;
   return cp;
}

unsigned char *
add_attr(int finished, size_t *outlen, int type, unsigned class, size_t length,
         unsigned b_value, void *v_value) {

   static int first_attr=1;
   unsigned char *attr;
   static unsigned char *attr_start=NULL;	/* Start of attr list */
   static size_t cur_offset;			/* Start of current attr */
   static size_t end_offset;			/* End of attr list */
   size_t len;					/* Attr length */
/*
 *	Construct a new attribute if we are not finalising.
 */
   if (!finished) {
      attr = make_attr(&len, type, class, length, b_value, v_value);
      if (first_attr) {
         cur_offset = 0;
         end_offset = len;
         attr_start = Malloc(end_offset);
         memcpy(attr_start, attr, len);
         first_attr = 0;
      } else {
         cur_offset = end_offset;
         end_offset += len;
         attr_start = Realloc(attr_start, end_offset);
         memcpy(attr_start+cur_offset, attr, len);
      }
      return NULL;
   } else {
      first_attr = 1;
      *outlen = end_offset;
      return attr_start;
   }
}

unsigned char*
make_transform2(size_t *length, unsigned next, unsigned trans_type,
                unsigned trans_id, unsigned char *attr, size_t attr_len) {

   struct isakmp_transform2* hdr;	/* Transform header */
   unsigned char *payload;
   unsigned char *cp;
   size_t len;				/* Payload Length */

/* Allocate and initialise the transform header */

   hdr = Malloc(sizeof(struct isakmp_transform2));
   memset(hdr, mbz_value, sizeof(struct isakmp_transform2));

   hdr->isat2_np = next;		/* Next payload type */
   hdr->isat2_transtype = trans_type;	/* Transform Type */
   hdr->isat2_transid = htons(trans_id);	/* Transform ID */

   len = attr_len + sizeof(struct isakmp_transform2);
   hdr->isat2_length = htons(len);	/* Transform length */
   *length = len;

/* Allocate memory for payload and copy structures to payload */

   payload = Malloc(len);

   cp = payload;
   memcpy(cp, hdr, sizeof(struct isakmp_transform2));
   free(hdr);
   cp += sizeof(struct isakmp_transform2);
   memcpy(cp, attr, attr_len);

   return payload;
}

unsigned char*
add_transform2(int finished, size_t *length, unsigned trans_type,
               unsigned trans_id, unsigned char *attr, size_t attr_len) {

   static int first_transform = 1;
   static unsigned char *trans_start=NULL;	/* Start of set of transforms */
   static size_t cur_offset;			/* Start of current transform */
   static size_t end_offset;			/* End of transforms */
   unsigned char *trans;			/* Transform payload */
   size_t len;					/* Transform length */
/*
 * Construct a transform if we are not finalising.
 * Set next to ISAKMP_NEXT_T (more transforms)
 */
   if (!finished) {
      trans = make_transform2(&len, ISAKMP_NEXT_T, trans_type, trans_id, attr,
                              attr_len);
      if (first_transform) {
         cur_offset = 0;
         end_offset = len;
         trans_start = Malloc(end_offset);
         memcpy(trans_start, trans, len);
         first_transform = 0;
      } else {
         cur_offset = end_offset;
         end_offset += len;
         trans_start = Realloc(trans_start, end_offset);
         memcpy(trans_start+cur_offset, trans, len);
      }
      free(trans);
      return NULL;
   } else {
      struct isakmp_transform2* hdr =
         (struct isakmp_transform2*) (trans_start+cur_offset);	/* Overlay */

      first_transform = 1;
      hdr->isat2_np = ISAKMP_NEXT_NONE;		/* No more transforms */
      *length = end_offset;
      return trans_start;
   }
}

unsigned char*
make_prop(size_t *outlen, unsigned next, unsigned number, unsigned notrans,
          unsigned protocol, unsigned spi_size, unsigned char *transforms,
          size_t transform_len) {
   unsigned char *payload;
   struct isakmp_proposal* hdr;
   unsigned char *cp;
   size_t len;

/* Allocate and initialise the proposal header */

   hdr = Malloc(sizeof(struct isakmp_proposal));
   memset(hdr, mbz_value, sizeof(struct isakmp_proposal));

   hdr->isap_np = next;
   hdr->isap_proposal = number;
   hdr->isap_protoid = protocol;
   hdr->isap_spisize = spi_size;	/* SPI Size */
   hdr->isap_notrans = notrans;		/* Number of transforms */

/* Determine total SA length and allocate payload memory */

   len = sizeof(struct isakmp_proposal) + spi_size + transform_len;
   hdr->isap_length = htons(len);	/* Proposal payload length */
   payload = Malloc(len);
   cp = payload;

/* Copy the proposal header to the payload */

   memcpy(cp, hdr, sizeof(struct isakmp_proposal));
   cp += sizeof(struct isakmp_proposal);
   free(hdr);

/* If the SPI size is non-zero, add a random SPI of the specified length */

   if (spi_size > 0) {
      unsigned i;

      for (i=0; i<spi_size; i++)
         *(cp++) = (unsigned char) random_byte();
   }

/* Add the transforms */

   memcpy(cp, transforms, transform_len);

   *outlen = len;
   return payload;
}

unsigned char*
add_prop(int finished, size_t *outlen,
         unsigned notrans, unsigned protocol, unsigned spi_size,
         unsigned char *transforms, size_t transform_len) {

   static int first_proposal = 1;
   static unsigned char *prop_start=NULL;	/* Start of set of proposals */
   static size_t cur_offset;			/* Start of current proposal */
   static size_t end_offset;			/* End of proposals */
   static unsigned prop_no=1;
   unsigned char *prop;			/* Proposal payload */
   size_t len;					/* Proposal length */
/*
 * Construct a proposal if we are not finalising.
 * Set next to ISAKMP_NEXT_P (more proposals), and increment prop_no for next
 * time round.
 */
   if (!finished) {
      prop = make_prop(&len, ISAKMP_NEXT_P, prop_no, notrans, protocol,
                       spi_size, transforms, transform_len);
      prop_no++;
      if (first_proposal) {
         cur_offset = 0;
         end_offset = len;
         prop_start = Malloc(end_offset);
         memcpy(prop_start, prop, len);
         first_proposal = 0;
      } else {
         cur_offset = end_offset;
         end_offset += len;
         prop_start = Realloc(prop_start, end_offset);
         memcpy(prop_start+cur_offset, prop, len);
      }
      free(prop);
      return NULL;
   } else {
      struct isakmp_proposal* hdr =
         (struct isakmp_proposal*) (prop_start+cur_offset);	/* Overlay */

      first_proposal = 1;
      hdr->isap_np = ISAKMP_NEXT_NONE;		/* No more proposals */
      *outlen = end_offset;
      return prop_start;
   }
}

unsigned char*
make_sa2(size_t *outlen, unsigned next,
         unsigned char *proposals, size_t proposal_len) {
   unsigned char *payload;
   struct isakmp_sa2* hdr;
   unsigned char *cp;
   size_t len;

   hdr = Malloc(sizeof(struct isakmp_sa2));
   memset(hdr, mbz_value, sizeof(struct isakmp_sa2));

   hdr->isasa2_np = next;		/* Next Payload Type */

   len = sizeof(struct isakmp_sa2) + proposal_len;
   hdr->isasa2_length = htons(len);		/* SA Payload length */
   payload = Malloc(len);
   cp = payload;

   memcpy(cp, hdr, sizeof(struct isakmp_sa2));
   cp += sizeof(struct isakmp_sa2);
   memcpy(cp, proposals, proposal_len);

   *outlen = len;
   return payload;
}

unsigned char*
make_isakmp_hdr(unsigned xchg, unsigned next, unsigned length,
                int header_version, int hdr_flags, unsigned hdr_msgid,
				unsigned char *icookie_data, size_t icookie_data_len,
				unsigned char *rcookie_data, size_t rcookie_data_len) {
   unsigned char *payload;
   struct isakmp_hdr* hdr;

   payload = Malloc(sizeof(struct isakmp_hdr));
   hdr = (struct isakmp_hdr*) payload;	/* Overlay header struct on payload */
   memset(hdr, mbz_value, sizeof(struct isakmp_hdr));

   hdr->isa_icookie[0] = 0xdeadbeef;	/* Initiator cookie */
   hdr->isa_icookie[1] = 0xdeadbeef;
   hdr->isa_rcookie[0] = 0;		/* Set responder cookie to 0 */
   hdr->isa_rcookie[1] = 0;
   if (icookie_data) {
      memcpy(hdr->isa_icookie, icookie_data, icookie_data_len);
   }
   if (rcookie_data) {
      memcpy(hdr->isa_rcookie, rcookie_data, rcookie_data_len);
   }
   hdr->isa_np = next;			/* Next Payload Type */
   hdr->isa_version = header_version;	/* v1.0 by default */
   hdr->isa_xchg = xchg;		/* Exchange type */
   hdr->isa_flags = hdr_flags;		/* Flags */
   hdr->isa_msgid = htonl(hdr_msgid);	/* Message ID */
   hdr->isa_length = htonl(length);	/* Total ISAKMP message length */

   return payload;
}

int
recvfrom_wto(int s, unsigned char *buf, size_t len, struct sockaddr *saddr,
             int tmo) {
	fd_set readset;
	struct timeval to;
	int n;
	socklen_t saddr_len;

	if (tmo < 0)
		tmo = 0;	/* Negative timeouts not allowed */
	to.tv_sec  = tmo/1000000;
	to.tv_usec = (tmo - 1000000*to.tv_sec);
	FD_ZERO(&readset);
	FD_SET(s, &readset);
	n = select(s+1, &readset, NULL, NULL, &to);
	if (n < 0) {
		if (errno == EINTR) {
			return -1;	/* Handle "Interrupted System call" as timeout */
		} else {
			err_sys("ERROR: select");
		}
	} else if (n == 0) {
		return -1;	/* Timeout reading from network */
	}
	saddr_len = sizeof(struct sockaddr);
	if ((n = recvfrom(s, buf, len, 0, saddr, &saddr_len)) < 0) {
		if (errno == ECONNREFUSED || errno == ECONNRESET) {
			/*
			 *	Treat connection refused and connection reset as timeout.
			 *	It would be nice to remove the associated host, but we can't because
			 *	we cannot tell which host the connection refused relates to.
			 */
			return -1;
		} else {
			err_sys("ERROR: recvfrom");
		}
	}

	return n;
}

unsigned char *
initialise_ike_sa_packet(size_t *packet_out_len, ike_packet_params *params) {
   unsigned char *hdr;		/* ISAKMP Header */
   unsigned char *sa;		/* Security Association */
   unsigned char *prop;		/* Proposal */
   unsigned char *transforms;	/* All transforms */
   unsigned char *certreq=NULL;
   unsigned char *vid=NULL;
   unsigned char *nonce=NULL;
   unsigned char *ke=NULL;	/* Key Exchange */
   unsigned char *invalid_ke=NULL;
   unsigned char *cp;
   unsigned char *packet_out;	/* Constructed IKE packet */
   size_t sa_len;
   size_t prop_len;
   size_t certreq_len;
   size_t vid_len;
   size_t trans_len;
   size_t nonce_len;
   size_t ke_len;
   size_t invalid_ke_len;
   size_t kx_data_len=0;
   unsigned no_trans=0;	/* Number of transforms */
   unsigned next_payload;
   unsigned header_len;	/* Length in ISAKMP header */

   *packet_out_len = 0;
   next_payload = ISAKMP_NEXT_NONE;
/*
 *	Certificate request payload (Optional)
 */
   if (params->cr_data) {
      certreq = make_cr(&certreq_len, next_payload, params->cr_data,
                        params->cr_data_len);
      *packet_out_len += certreq_len;
      next_payload = ISAKMP_NEXT_CR;
   }
/*
 *	Vendor ID Payload (Optional)
 */
   if (params->vendor_id_flag) {
      vid = add_vid(1, &vid_len, NULL, 0, 2, next_payload);
      *packet_out_len += vid_len;
      next_payload = ISAKMP_NEXT_V2_VID;
   }

/* IKEv2 Key Exchange and Nonce Payloads */

   if (params->nonce_presence) {
	   nonce = make_nonce(&nonce_len, next_payload, params->nonce_data_len);
	   *packet_out_len += nonce_len;
	   next_payload = ISAKMP_NEXT_V2_NONCE;
	   switch (params->dhgroup) {
		  case 1:
			 kx_data_len = 96;	/* Group 1 - 768 bits */
			 break;
		  case 2:
			 kx_data_len = 128;	/* Group 2 - 1024 bits */
			 break;
		  case 5:
			 kx_data_len = 192;	/* Group 5 - 1536 bits */
			 break;
		  case 14:
			 kx_data_len = 256;	/* Group 14 - 2048 bits */
			 break;
		  case 15:
			 kx_data_len = 384;	/* Group 15 - 3072 bits */
			 break;
		  case 16:
			 kx_data_len = 512;	/* Group 16 - 4096 bits */
			 break;
		  case 17:
			 kx_data_len = 768;	/* Group 17 - 6144 bits */
			 break;
		  case 18:
			 kx_data_len = 1024;	/* Group 18 - 8192 bits */
			 break;
		  case 19:
			 kx_data_len = 64;	/* Group 19 - 256+256 bits */
			 break;
		  case 20:
			 kx_data_len = 96;	/* Group 20 - 384+384 bits */
			 break;
		  case 21:
			 kx_data_len = 132;	/* Group 21 - 528+528 bits */
			 break;
		  default:
			 err_msg("ERROR: Bad Diffie Hellman group: %u, "
					 "should be 1,2,5,14,15,16,17,18,19,20 or 21",
					 params->dhgroup);	/* Doesn't return */
	   }
   }
   if (params->ke_presence) {
	   ke = make_ke2(&ke_len, next_payload, params->dhgroup, kx_data_len);
	   *packet_out_len += ke_len;
	   next_payload = ISAKMP_NEXT_V2_KE;
   }
/*
 *	Transform payloads
 */
   if (params->sa_presence) {
	   unsigned char *attr;
	   size_t attr_len;

	   add_attr(0, NULL, 'B', OAKLEY_KEY_LENGTH, 0, 256, NULL);
	   attr = add_attr(1, &attr_len, '\0', 0, 0, 0, NULL);
	   add_transform2(0, NULL, IKEV2_TYPE_ENCR, IKEV2_ENCR_AES_CBC, attr, attr_len);
	   free(attr);
	   add_attr(0, NULL, 'B', OAKLEY_KEY_LENGTH, 0, 128, NULL);
	   attr = add_attr(1, &attr_len, '\0', 0, 0, 0, NULL);
	   add_transform2(0, NULL, IKEV2_TYPE_ENCR, IKEV2_ENCR_AES_CBC, attr, attr_len);
	   free(attr);
	   add_transform2(0, NULL, IKEV2_TYPE_ENCR, IKEV2_ENCR_3DES, NULL, 0);
	   add_transform2(0, NULL, IKEV2_TYPE_ENCR, IKEV2_ENCR_DES, NULL, 0);
	   add_transform2(0, NULL, IKEV2_TYPE_PRF, IKEV2_PRF_HMAC_SHA1, NULL, 0);
	   add_transform2(0, NULL, IKEV2_TYPE_PRF, IKEV2_PRF_HMAC_MD5, NULL, 0);
	   add_transform2(0, NULL, IKEV2_TYPE_INTEG, IKEV2_AUTH_HMAC_SHA1_96, NULL, 0);
	   add_transform2(0, NULL, IKEV2_TYPE_INTEG, IKEV2_AUTH_HMAC_MD5_96, NULL, 0);
	   add_transform2(0, NULL, IKEV2_TYPE_DH, 2, NULL, 0);
	   add_transform2(0, NULL, IKEV2_TYPE_DH, 5, NULL, 0);
	   add_transform2(0, NULL, IKEV2_TYPE_DH, 14, NULL, 0);
	   transforms = add_transform2(1, &trans_len, 0, 0, NULL, 0);
	   no_trans=11;
	   /*
		*	Proposal payload
		*/
	   add_prop(0, NULL, no_trans, params->protocol, params->spi_size, transforms,
			   trans_len);
	   prop = add_prop(1, &prop_len, 0, 0, 0, NULL, 0);
	   free(transforms);
	   /*
		*	SA payload
		*/
	   sa = make_sa2(&sa_len, next_payload, prop, prop_len);
	   next_payload = ISAKMP_NEXT_V2_SA;
	   *packet_out_len += sa_len;
	   free(prop);
   }
/*
 *	Invalid KE
 */
   if (params->invalid_ke_presence)
   {
	   invalid_ke = make_notify_invalid_ke(&invalid_ke_len, next_payload, params->invalid_ke_dh);
	   next_payload = ISAKMP_NEXT_V2_N;
	   *packet_out_len += invalid_ke_len;
   }
/*
 *	ISAKMP Header
 */
   *packet_out_len += sizeof(struct isakmp_hdr);
   header_len = *packet_out_len;	/* Set header len to correct value */
   if (params->header_length) {	/* Manually specify header length */
      char *temp_cp;

      temp_cp = params->header_length;
      if (*temp_cp == '+') {
         header_len += Strtoul(++temp_cp, 0);
      } else if (*temp_cp == '-') {
         header_len -= Strtoul(++temp_cp, 0);
      } else {
         header_len = Strtoul(temp_cp, 0);
      }
   }
   if (params->hdr_next_payload) {	/* Manually specify next payload */
      next_payload = params->hdr_next_payload;
   }
   hdr = make_isakmp_hdr(params->exchange_type, next_payload,
                         header_len, params->header_version,
                         params->hdr_flags, params->hdr_msgid,
						 params->icookie_data, params->icookie_data_len,
                         params->rcookie_data, params->rcookie_data_len);
/*
 *	Allocate packet and copy payloads into packet.
 */
   packet_out=Malloc(*packet_out_len);
   cp = packet_out;
   memcpy(cp, hdr, sizeof(struct isakmp_hdr));
   free(hdr);
   cp += sizeof(struct isakmp_hdr);
   if (params->sa_presence) {
	   memcpy(cp, sa, sa_len);
	   free(sa);
	   cp += sa_len;
   }
   if (params->ke_presence) {
	   memcpy(cp, ke, ke_len);
	   free(ke);
	   cp += ke_len;
   }
   if (params->nonce_presence) {
	   memcpy(cp, nonce, nonce_len);
	   free(nonce);
	   cp += nonce_len;
   }
   if (params->vendor_id_flag) {
      memcpy(cp, vid, vid_len);
      free(vid);
      cp += vid_len;
   }
   if (params->cr_data) {
      memcpy(cp, certreq, certreq_len);
      free(certreq);
      cp += certreq_len;
   }
   if (params->invalid_ke_presence) {
	   memcpy(cp, invalid_ke, invalid_ke_len);
	   free(invalid_ke);
	   cp += invalid_ke_len;
   }

   return packet_out;
}

void
send_packet(int s, unsigned char *packet_out, size_t packet_out_len,
            struct in_addr *source_ip, unsigned source_port,
			struct in_addr *dest_ip, unsigned dest_port) {
   struct sockaddr_in sa_peer;
   socklen_t sa_peer_len;
   int nsent;
/*
 *	Set up the sockaddr_in structure for the host.
 */
   memset(&sa_peer, '\0', sizeof(sa_peer));
   sa_peer.sin_family = AF_INET;
   sa_peer.sin_addr.s_addr = dest_ip->s_addr;
   sa_peer.sin_port = htons(dest_port);
   sa_peer_len = sizeof(sa_peer);
/*
 *	Spoof source address
 */
   if (source_ip != NULL) {
      unsigned char *orig_packet_out = packet_out;
      size_t orig_packet_out_len = packet_out_len;
      struct iphdr *iph;
      struct udphdr *udph;
      struct pseudo_hdr *pseudo;
      uint32_t source_address;

	  source_address = source_ip->s_addr;
      packet_out=Malloc(sizeof(struct iphdr) + sizeof(struct udphdr) +
                        packet_out_len);
      iph = (struct iphdr *) packet_out;
      udph = (struct udphdr *) (packet_out + sizeof(struct iphdr));
      pseudo = (struct pseudo_hdr *) (packet_out + sizeof(struct iphdr) -
                                      sizeof(struct pseudo_hdr));
/*
 *	Copy the data to the new buffer, leaving space for the IP and
 *	UDP headers.
 */
      memcpy(packet_out + sizeof(struct iphdr) + sizeof(struct udphdr),
             orig_packet_out, packet_out_len);
      packet_out_len += sizeof(struct iphdr) + sizeof(struct udphdr);
/*
 *      Construct the pseudo header (for UDP checksum purposes).
 *      Note that this overlaps the IP header and gets overwritten later.
 */
      memset(pseudo, '\0', sizeof(struct pseudo_hdr));
      pseudo->src_addr = source_address;
      pseudo->dst_addr = dest_ip->s_addr;
      pseudo->proto    = 17;	/* UDP */
      pseudo->length   = htons(sizeof(struct udphdr) + orig_packet_out_len);
/*
 *      Construct the UDP header.
 */
      memset(udph, '\0', sizeof(struct udphdr));
      udph->source = htons(source_port);
      udph->dest = htons(dest_port);
      udph->len = htons(sizeof(struct udphdr) + orig_packet_out_len);
      udph->check = in_cksum((uint16_t *)pseudo, sizeof(struct pseudo_hdr) +
                             sizeof(struct udphdr) + orig_packet_out_len);
/*
 *      Construct the IP Header.
 *      This overwrites the now unneeded pseudo header.
 */
      memset(iph, '\0', sizeof(struct iphdr));
      iph->ihl = 5;        /* 5 * 32-bit longwords = 20 bytes */
      iph->version = 4;
      iph->tos = 0;
      iph->tot_len = packet_out_len;
      iph->id = 0;         /* Linux kernel fills this in */
      iph->frag_off = htons(0x0);
      iph->ttl = 128;
      iph->protocol = 17;	/* UDP */
      iph->check = 0;      /* Linux kernel fills this in */
      iph->saddr = source_address;
      iph->daddr = dest_ip->s_addr;
   }
/*
 *	Send the packet.
 */
      nsent = sendto(s, packet_out, packet_out_len, 0,
                     (struct sockaddr *) &sa_peer, sa_peer_len);
   if (nsent < 0) {
      err_sys("ERROR: sendto");
   } else if ((unsigned)nsent != packet_out_len) {
      warn_msg("WARNING: sendto: only %d bytes sent, but %u requested",
               nsent, packet_out_len);
   }
/*
 *	Free locally allocated memory if required
 */
   if (source_ip != NULL) {
      free(packet_out);
   }
}

void
display_packet(int n, unsigned char *packet_in) {
   char *cp;			/* Temp pointer */
   size_t bytes_left;		/* Remaining buffer size */
   unsigned next;		/* Next Payload */
   unsigned type;		/* Exchange Type */
   char *msg2;
   unsigned char *pkt_ptr;
   char *hdr_descr;		/* ISAKMP header description */
/*
 *	Process ISAKMP header.
 *	If this returns zero length left, indicating some sort of problem, then
 *	we report a short or malformed packet and return.
 *	If the processing is successful, pkt_ptr points to the next payload.
 */
   struct isakmp_hdr * hdr = (struct isakmp_hdr *)packet_in;

   printf("IKEv2 header:\n");
   msg2 = hexstring((unsigned char *)hdr->isa_icookie, 8);
   printf("\tInitiator cooke: '%s'\n", msg2);
   free(msg2);
   msg2 = hexstring((unsigned char *)hdr->isa_rcookie, 8);
   printf("\tResponder cooke: '%s'\n", msg2);
   free(msg2);
   printf("\tversion:'0x%.2x'", hdr->isa_version);
   printf("\tflags: '0x%.2x'", hdr->isa_flags);
   printf("\tMsgid: '%.8x'", ntohl(hdr->isa_msgid));
   printf("\tPacket type: '%d'", hdr->isa_xchg);
/*
 *	There is another payload after this one, so adjust length and
 *	return pointer to next payload.
 */
   bytes_left = hdr->isa_length - sizeof(struct isakmp_hdr);
   next = hdr->isa_np;

   if (!bytes_left) {
      printf("Short or malformed ISAKMP packet returned: %d bytes\n", n);
      return;
   }
/*
 *	Determine the overall type of the packet from the first payload type.
 *	We assume that pkt_ptr is suitably aligned because the ISAKMP header
 *	has a fixed length that is divisible by 4.
 */
//   switch (next) {
//      case ISAKMP_NEXT_V2_SA:	/* IKEv2 SA */
//         cp = process_sa2(pkt_ptr, bytes_left, type, quiet, multiline,
//                          hdr_descr);
//         break;
//      case ISAKMP_NEXT_V2_N:	/* IKEv2 Notify */
//         cp = process_notify2(pkt_ptr, bytes_left, quiet, multiline,
//                              hdr_descr);
//         break;
//
//      case ISAKMP_NEXT_VID:	/* Vendor ID */
//      case ISAKMP_NEXT_V2_VID:	/* IKEv2 Vendor ID */
//         cp = process_vid(payload_ptr, bytes_left, vidlist);
//         break;
//      case ISAKMP_NEXT_ID:	/* ID */
//         cp = process_id(payload_ptr, bytes_left);
//         break;
//      case ISAKMP_NEXT_CERT:	/* Certificate */
//      case ISAKMP_NEXT_CR:	/* Certificate Request */
//         cp = process_cert(payload_ptr, bytes_left, next);
//         break;
//      case ISAKMP_NEXT_D:		/* Delete */
//         cp = process_delete(payload_ptr, bytes_left);
//         break;
//      case ISAKMP_NEXT_N:		/* Notification */
//         cp = process_notification(payload_ptr, bytes_left);
//         break;
//      default:			/* Something else */
//         cp = process_generic(payload_ptr, bytes_left, next);
//         break;
//      default:			/* Something else */
//         cp=make_message("Unexpected IKE payload returned: %s",
//                         id_to_name(next, payload_map));
//         break;
//   }
}

int main(int argc, char *argv[])
{
	int source_port = 500;
	int dest_port = 500;
	struct in_addr source_ip, sourcebind_ip, dest_ip;
	int sourceip_flag = 1;
	int sourceipbind_flag = 0;
	struct ifreq ifr;

    struct sockaddr_in sa_peer;
	int n;
	int sockfd;
	struct sockaddr_in sa_local;
	const int on = 1;	/* for setsockopt() */
	const int off = 0;	/* for setsockopt() */
	uint8_t packet_in[MAXUDP];

	assert(argc == 2 || argc == 3);
	if (argc == 3)
		sourceipbind_flag = 1;

	inet_aton("0.0.0.0", &source_ip);
	//assert(inet_aton("1.2.3.4", &sourcebind_ip));
	if (!inet_aton(argv[argc-1], &dest_ip))
			err_sys("ERROR: wrong destination address");

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		err_sys("ERROR: socket");
	if ((setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on))) != 0)
		err_sys("setsockopt");

	 if (sourceip_flag) {	/* Raw IP socket */
      const int on = 1;	/* for setsockopt() */

      //if ((sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
      if ((sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
         err_sys("socket");
      if ((setsockopt(sockfd, IPPROTO_IP, IP_HDRINCL, &on, sizeof(on))) != 0)
         err_sys("setsockopt");
      if ((setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on))) != 0)
         err_sys("setsockopt");
   } else {
      const int on = 1;	/* for setsockopt() */

      if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
         err_sys("ERROR: socket");
      if ((setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on))) != 0)
         err_sys("setsockopt");
   }

	 if (argc == 3) {
		 if (sscanf(argv[1], "%d.%d.%d.%d%c", &(int){0}, &(int){0}, &(int){0}, &(int){0}, &(char){0}) == 4) {
			if (!inet_aton(argv[1], &sourcebind_ip))
				err_sys("ERROR: wrong source address");

			memset(&sa_local, '\0', sizeof(sa_local));
			sa_local.sin_family = AF_INET;
			if (sourceipbind_flag)
				sa_local.sin_addr = sourcebind_ip;
			else
				sa_local.sin_addr.s_addr = htonl(INADDR_ANY);
			sa_local.sin_port = htons(source_port);

			if ((bind(sockfd, (struct sockaddr *)&sa_local, sizeof(sa_local))) < 0) {
				warn_msg("ERROR: Could not bind network socket to local port %u", source_port);
				if (errno == EACCES)
					warn_msg("You need to be root, or ike-scan must be suid root to bind to ports below 1024.");
				if (errno == EADDRINUSE)
					warn_msg("Only one process may bind to the source port at any one time.");
				err_sys("ERROR: bind");
			}
		 }
		 else {
			 memset(&ifr, 0, sizeof(ifr));
			 strlcpy(ifr.ifr_name, argv[1], sizeof(ifr.ifr_name));
			 if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr)) < 0)
				err_sys("ERROR: bind");
		 }
	}


	size_t packet_out_len;
	//unsigned char *packet_out=initialise_ike_sa_packet(&packet_out_len, &sa_params);
	unsigned char *packet_out=initialise_ike_sa_packet(&packet_out_len, &sa_params);


	for (uint64_t i = 1; i <= UINT64_MAX; ++i) {
		source_ip.s_addr = i;
		send_packet(sockfd, packet_out, packet_out_len,
				/*&source_ip*/ (sourceip_flag?&source_ip:NULL), source_port, &dest_ip, dest_port);
	}

	if (!sourceip_flag)
	{
      n=recvfrom_wto(sockfd, packet_in, sizeof(packet_in), (struct sockaddr *)&sa_peer,
                     DEFAULT_TIMEOUT);

      if (n != -1) {
               display_packet(n, packet_in);
      } /* End If */
	}

	close(sockfd);

	return 0;
}
